import sequelize from '../db/connection';
import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = (bool, entity) => `CASE WHEN "${entity}Reactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      sortBy,
      order: sortOrder,
      userId
    } = filter;

    const order = [];
    if (sortBy !== 'createdAt') {
      switch (sortBy) {
        case 'likeCount': {
          order.push([sequelize.fn('SUM', sequelize.literal(likeCase(true, 'post'))), sortOrder]);
          break;
        }
        case 'dislikeCount': {
          order.push([sequelize.fn('SUM', sequelize.literal(likeCase(false, 'post'))), sortOrder]);
          break;
        }
        default: { break; }
      }
    }
    order.push(['createdAt', sortOrder]);

    const where = {};
    if (userId) {
      Object.assign(where, { userId });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true, 'post'))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false, 'post'))), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order,
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(true, 'post'))), 'likeCount'],
          [sequelize.fn('SUM', sequelize.literal(likeCase(false, 'post'))), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        attributes: {
          include: [
            [sequelize.fn('SUM', sequelize.literal(likeCase(true, 'comments->comment'))), 'likeCount'],
            [sequelize.fn('SUM', sequelize.literal(likeCase(false, 'comments->comment'))), 'dislikeCount']
          ]
        },
        include: [{
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }, {
          model: CommentReactionModel,
          attributes: []
        }]
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }
}

export default new PostRepository(PostModel);
