import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.comment && (reaction.comment.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked his post
        req.io.to(reaction.comment.userId).emit('like', 'Your comment was liked!');
      }
      return commentService.getCommentById(req.body.commentId);
    })
    .then(comment => res.send(comment))
    .catch(next))
  .put('/:id', (req, res, next) => commentService.updateById(req.params.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteById(req.params.id)
    .then(() => res.sendStatus(200))
    .catch(next));

export default router;
