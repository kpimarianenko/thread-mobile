import React from 'react';
import { connect } from 'react-redux';
import { Text, View } from 'react-native';
import Logo from '../../components/Logo';
import LoginForm from '../../components/LoginForm';
import style from '../../styles/auth.style';
import ToggleAuthScreen from '../../components/ToggleAuthScreen';
import routes from '../../routes/routeNames';
import * as actions from '../Profile/actions';

const Login = ({
  navigation, loginSuccess, loginFailure, authorizing, authorized, isLoading, error
}) => {
  const moveToRegistration = () => {
    navigation.navigate(routes.auth.REGISTRATION);
  };

  return (
    <View style={style.page}>
      <Logo />
      <Text style={style.title}>Login to your account</Text>
      <LoginForm
        isLoading={isLoading}
        error={error}
        loginFailure={loginFailure}
        loginSuccess={loginSuccess}
        authorizing={authorizing}
        authorized={authorized}
      />
      <ToggleAuthScreen
        text="New to us?"
        btnText="Sign Up"
        onPress={moveToRegistration}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  ...state.profile
});

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
