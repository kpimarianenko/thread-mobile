import React from 'react';
import { connect } from 'react-redux';
import { Text, View } from 'react-native';
import Logo from '../../components/Logo';
import RegistrationForm from '../../components/RegistrationForm';
import ToggleAuthScreen from '../../components/ToggleAuthScreen';
import style from '../../styles/auth.style';
import * as actions from '../Profile/actions';

const Registration = ({
  navigation, loginSuccess, loginFailure, authorizing, authorized, error, isLoading
}) => {
  const moveToLogin = () => {
    navigation.goBack();
  };

  return (
    <View style={style.page}>
      <Logo />
      <Text style={style.title}>Register for free account</Text>
      <RegistrationForm
        isLoading={isLoading}
        error={error}
        loginFailure={loginFailure}
        loginSuccess={loginSuccess}
        authorizing={authorizing}
        authorized={authorized}
      />
      <ToggleAuthScreen
        text="Already with us?"
        btnText="Sign In"
        onPress={moveToLogin}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  ...state.profile
});

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Registration);
