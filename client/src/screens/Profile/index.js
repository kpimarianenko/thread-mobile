import React from 'react';
import { connect } from 'react-redux';
import { View, Image } from 'react-native';
import ErrorInput from '../../components/ErrorInput';
import AvaImg from '../../assets/images/ava-placeholder.png';
import EmailImg from '../../assets/icons/email.png';
import UsernameImg from '../../assets/icons/username.png';
import style from './style';

const Profile = ({ user }) => (
  <View style={style.profile}>
    <Image style={style.ava} source={user.image ? user.image.link : AvaImg} />
    <ErrorInput
      editable={false}
      value={user.email}
      source={EmailImg}
    />
    <ErrorInput
      editable={false}
      value={user.username}
      source={UsernameImg}
    />
  </View>
);

const mapStateToProps = (state) => ({
  ...state.profile
});

export default connect(mapStateToProps)(Profile);
