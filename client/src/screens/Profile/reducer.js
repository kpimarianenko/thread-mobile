import { SET_USER_DATA, SET_IS_LOADING } from './actionTypes';

const initialState = {
  token: null,
  user: null,
  isLoading: false,
  error: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_USER_DATA: {
      const { token, user, error } = action.payload;
      return {
        ...state,
        token,
        user,
        error
      };
    }
    case SET_IS_LOADING: {
      const { isLoading } = action.payload;
      return {
        ...state,
        isLoading
      };
    }
    default: {
      return state;
    }
  }
}
