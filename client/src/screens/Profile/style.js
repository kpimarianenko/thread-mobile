import { StyleSheet, Dimensions } from 'react-native';
import baseStyle, { variables } from '../../styles/base.style';

const windowWidth = Dimensions.get('window').width;

const style = StyleSheet.create({
  profile: {
    ...baseStyle.maximized,
    alignItems: 'center',
    padding: variables.gap,
    paddingHorizontal: variables.gap * 3
  },
  ava: {
    width: windowWidth * 0.8,
    height: windowWidth * 0.8,
    borderRadius: windowWidth,
    marginBottom: variables.gap
  }
});

export default style;
