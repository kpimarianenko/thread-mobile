import { SET_USER_DATA, SET_IS_LOADING } from './actionTypes';

const setUserData = (data, error) => ({
  type: SET_USER_DATA,
  payload: {
    token: data.token,
    user: data.user,
    error
  }
});

const setIsLoading = (isLoading) => ({
  type: SET_IS_LOADING,
  payload: {
    isLoading
  }
});

export const authorized = () => setIsLoading(false);

export const authorizing = () => setIsLoading(true);

export const loginSuccess = (data) => setUserData(data);

export const loginFailure = (error) => setUserData({ user: null, token: null }, error);

export const logout = () => setUserData({ user: null, token: null });
