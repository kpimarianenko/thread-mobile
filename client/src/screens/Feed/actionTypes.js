export const SET_POSTS = 'FEED_ACTION:SET_POSTS';
export const ADD_POST = 'FEED_ACTION:ADD_POST';
export const UPDATE_POST = 'FEED_ACTION:UPDATE_POST';
export const DELETE_POST = 'FEED_ACTION:DELETE_POST';
export const SET_IS_LOADING = 'FEED_ACTION:SET_IS_LOADING';
export const SET_ERROR = 'FEED_ACTION:SET_ERROR';
