import {
  SET_POSTS, ADD_POST, UPDATE_POST, DELETE_POST, SET_IS_LOADING, SET_ERROR
} from './actionTypes';

const initialState = {
  posts: [],
  error: null,
  isLoading: true
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_POSTS: {
      const { posts, error } = action.payload;
      return {
        ...state,
        posts,
        error
      };
    }
    case SET_ERROR: {
      const { error } = action.payload;
      return {
        ...state,
        error
      };
    }
    case ADD_POST: {
      const { post } = action.payload;
      return {
        ...state,
        posts: [post, ...state.posts]
      };
    }
    case UPDATE_POST: {
      const { post } = action.payload;
      const posts = state.posts.map((p) => (p.id === post.id ? { ...p, ...post } : p));
      return {
        ...state,
        posts
      };
    }
    case DELETE_POST: {
      const { id } = action.payload;
      const posts = state.posts.filter((p) => (p.id !== id));
      return {
        ...state,
        posts
      };
    }
    case SET_IS_LOADING: {
      const { isLoading } = action.payload;
      return {
        ...state,
        isLoading
      };
    }
    default: {
      return state;
    }
  }
}
