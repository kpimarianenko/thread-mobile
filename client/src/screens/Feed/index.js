import React, { useState, useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { connect } from 'react-redux';
import { View, Switch, Text } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import moment from 'moment';
import PostForm from '../../components/PostForm';
import PostList from '../../components/PostList';
import style from './style';
import * as postService from '../../services/postService';
import * as actions from './actions';
import routeNames from '../../routes/routeNames';
import Loader from '../../components/Loader';
import { setReaction } from '../../services/postService';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10,
  sortBy: 'createdAt',
  order: 'DESC'
};

const Feed = (props) => {
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [sortBy, setSortBy] = useState(postsFilter.sortBy);
  const [sortOrder, setSortOrder] = useState(postsFilter.order);
  const {
    navigation, posts, user, isLoading, error
  } = props;

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    postsFilter.userId = postsFilter.userId ? undefined : user.id;
    postsFilter.from = postsFilter.count;
  };

  const changeSortBy = (value) => {
    setSortBy(value);
    postsFilter.sortBy = value;
  };

  const changeSortOrder = (value) => {
    setSortOrder(value);
    postsFilter.order = value;
  };

  const getAndSetPosts = (curPosts = []) => {
    if (posts.length < postsFilter.from) return;
    props.startPostsLoading();
    postService.getPosts(postsFilter)
      .then((res) => props.setPostsSuccess([...curPosts, ...res.data]))
      .catch((err) => props.setPostsFailure(err))
      .finally(props.postsLoaded);
    postsFilter.from += postsFilter.count;
  };

  const loadMore = () => {
    getAndSetPosts(posts);
  };

  const load = () => {
    postsFilter.from = 0;
    getAndSetPosts();
  };

  const setLike = (id) => {
    setReaction(id, true)
      .then((res) => props.updatePost(res.data))
      .catch((err) => props.setPostFailure(err.response.data.message));
  };

  const setDislike = (id) => {
    setReaction(id, false)
      .then((res) => props.updatePost(res.data))
      .catch((err) => props.setPostFailure(err.response.data.message));
  };

  useFocusEffect(useCallback(
    () => {
      load();
    },
    [showOwnPosts, sortBy, sortOrder]
  ));

  const header = () => (
    <>
      <PostForm
        user={user}
        error={error}
        setError={props.setError}
        startPostsLoading={props.startPostsLoading}
        postsLoaded={props.postsLoaded}
        isLoading={isLoading}
        addPost={(p) => {
          load();
          props.addPost(p);
        }}
      />
      <View style={style.filters}>
        <Picker style={style.picker} selectedValue={sortBy} onValueChange={changeSortBy}>
          <Picker.Item label="Date" value="createdAt" />
          <Picker.Item label="Likes" value="likeCount" />
          <Picker.Item label="Dislikes" value="dislikeCount" />
        </Picker>
        <Picker style={style.picker} selectedValue={sortOrder} onValueChange={changeSortOrder}>
          <Picker.Item label="Descending" value="DESC" />
          <Picker.Item label="Ascending" value="ASC" />
        </Picker>
      </View>
      <View style={style.filters}>
        <Switch
          value={showOwnPosts}
          onValueChange={toggleShowOwnPosts}
        />
        <Text>Show only my posts</Text>
      </View>
    </>
  );

  return (
    <View style={style.feed}>
      <PostList
        ListHeaderComponent={header}
        ListFooterComponent={isLoading && <Loader />}
        onEndReached={loadMore}
        onEndReachedThreshold={0.5}
        initialNumToRender={postsFilter.count}
        openDetails={(id) => navigation.navigate(routeNames.main.POST, { id })}
        setLike={setLike}
        setDislike={setDislike}
        posts={posts.sort((c1, c2) => moment(c1.createdAt).fromNow(c2.createdAt))}
        isLoading={isLoading}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  user: state.profile.user,
  ...state.feed
});

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Feed);
