import { StyleSheet } from 'react-native';
import baseStyle, { variables } from '../../styles/base.style';

const style = StyleSheet.create({
  feed: {
    padding: variables.gap
  },
  filters: {
    ...baseStyle.row,
    marginBottom: variables.gap
  },
  picker: {
    height: 50,
    flex: 1
  }
});

export default style;
