import {
  SET_POSTS, ADD_POST, UPDATE_POST, DELETE_POST, SET_ERROR, SET_IS_LOADING
} from './actionTypes';

const setPosts = (posts, error) => ({
  type: SET_POSTS,
  payload: {
    posts,
    error
  }
});

export const setError = (error) => ({
  type: SET_ERROR,
  payload: {
    error
  }
});

export const addPost = (post) => ({
  type: ADD_POST,
  payload: {
    post
  }
});

export const updatePost = (post) => ({
  type: UPDATE_POST,
  payload: {
    post
  }
});

export const deletePost = (id) => ({
  type: DELETE_POST,
  payload: {
    id
  }
});

const setIsLoading = (isLoading) => ({
  type: SET_IS_LOADING,
  payload: {
    isLoading
  }
});

export const setPostsSuccess = (posts) => setPosts(posts);

export const setPostsFailure = (error) => setPosts([], error);

export const postsLoaded = () => setIsLoading(false);

export const startPostsLoading = () => setIsLoading(true);
