import { StyleSheet } from 'react-native';
import baseStyle, { variables } from '../../styles/base.style';

const style = StyleSheet.create({
  wrapper: {
    flex: 1,
    padding: variables.gap
  },
  post: {
    ...baseStyle.container,
    padding: variables.gap
  },
  warn: {
    ...baseStyle.centerText,
    marginBottom: variables.gap
  },
  commentInput: {
    flexDirection: 'row',
    height: 100
  }
});

export default style;
