import {
  SET_POST, ADD_COMMENT, SET_CURRENT_COMMENT, UPDATE_COMMENT, DELETE_COMMENT
} from './actionTypes';

const setPost = (post, error) => ({
  type: SET_POST,
  payload: {
    error,
    post
  }
});

export const addComment = (comment) => ({
  type: ADD_COMMENT,
  payload: {
    comment
  }
});

export const updateComment = (comment) => ({
  type: UPDATE_COMMENT,
  payload: {
    comment
  }
});

export const deleteComment = (id) => ({
  type: DELETE_COMMENT,
  payload: {
    id
  }
});

export const setCurrentComment = (id) => ({
  type: SET_CURRENT_COMMENT,
  payload: {
    id
  }
});

export const setPostSuccess = (post) => setPost(post);

export const setPostFailure = (error) => setPost(null, error);
