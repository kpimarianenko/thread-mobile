import React, { useEffect, useState } from 'react';
import {
  FlatList, Text, View, Alert
} from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';
import Comment from '../../components/Comment';
import CommentForm from '../../components/CommentForm';
import CommentsListHeader from '../../components/CommentsListHeader';
import DeleteModal from '../../components/DeleteModal';
import EditModal from '../../components/EditModal';
import * as actions from './actions';
import * as deleteModalActions from '../../components/DeleteModal/actions';
import * as editModalActions from '../../components/EditModal/actions';
import * as feedActions from '../Feed/actions';
import * as postService from '../../services/postService';
import * as commentService from '../../services/commentService';
import style from './style';

const ExtendedPost = (props) => {
  const {
    navigation, route, user, post, curComment, error
  } = props;
  const [isModalModePost, setModalModePost] = useState(true);

  const alert = (message) => {
    Alert.alert('Error', message);
  };

  const likePost = () => {
    postService.setReaction(post.id, true)
      .then((res) => props.setPostSuccess(res.data))
      .catch((err) => props.setPostFailure(err.response.data.message));
  };

  const dislikePost = () => {
    postService.setReaction(post.id, false)
      .then((res) => props.setPostSuccess(res.data))
      .catch((err) => props.setPostFailure(err.response.data.message));
  };

  const likeComment = (id) => {
    commentService.setReaction(id, true)
      .then((res) => props.updateComment(res.data))
      .catch((err) => alert(err.response.data.message));
  };

  const dislikeComment = (id) => {
    commentService.setReaction(id, false)
      .then((res) => props.updateComment(res.data))
      .catch((err) => alert(err.response.data.message));
  };

  const sendComment = (c) => {
    if (c.length > 0) {
      commentService.addComment(c, post.id)
        .then((res) => {
          props.addComment({
            ...res.data,
            user
          });
          props.updatePost({
            id: post.id,
            commentCount: +post.commentCount + 1
          });
        })
        .catch((err) => alert(err.response.data.message));
    }
  };

  const onPostUpdate = (id, value) => {
    if (value.length > 0) {
      postService.updatePost(id, value)
        .then((res) => {
          const newPost = {
            ...post,
            ...res.data
          };
          props.setPostSuccess(newPost);
          props.updatePost(newPost);
        })
        .catch((err) => props.setPostFailure(err.response.data.message));
      props.hideEditWindow();
    }
  };

  const onPostDelete = () => {
    postService.deletePost(post.id)
      .then(() => {
        props.deletePost(post.id);
        navigation.goBack();
      })
      .catch((err) => alert(err.response.data.message));
    props.hideDeleteWindow();
  };

  const onCommentUpdate = (id, value) => {
    if (value.length > 0) {
      commentService.updateComment(id, value)
        .then((res) => props.updateComment(res.data))
        .catch((err) => alert(err.response.data.message));
      props.hideEditWindow();
    }
  };

  const onCommentDelete = () => {
    commentService.deleteComment(curComment.id)
      .then(() => {
        props.deleteComment(curComment.id);
        props.updatePost({
          id: post.id,
          commentCount: +post.commentCount - 1
        });
      })
      .catch((err) => alert(err.response.data.message));
    props.hideDeleteWindow();
  };

  const editModalConfirm = (value) => {
    if (isModalModePost) {
      onPostUpdate(post.id, value);
    } else {
      onCommentUpdate(curComment.id, value);
    }
  };

  useEffect(() => {
    postService.getPostById(route.params.id)
      .then((res) => props.setPostSuccess(res.data))
      .catch((err) => props.setPostFailure(err.response.data.message));
  }, []);

  const header = () => (
    <CommentsListHeader
      onEdit={() => {
        setModalModePost(true);
        props.showEditWindow();
      }}
      onDelete={() => {
        setModalModePost(true);
        props.showDeleteWindow();
      }}
      setLike={likePost}
      setDislike={dislikePost}
      user={user}
      post={post}
    />
  );

  const renderComment = ({ item }) => (
    <Comment
      onEdit={() => {
        props.showEditWindow();
        props.setCurrentComment(item.id);
        setModalModePost(false);
      }}
      onDelete={() => {
        props.showDeleteWindow();
        props.setCurrentComment(item.id);
        setModalModePost(false);
      }}
      user={user}
      comment={item}
      setLike={() => likeComment(item.id)}
      setDislike={() => dislikeComment(item.id)}
    />
  );

  if (error) return <Text style={style.warn}>{error}</Text>;
  return post ? (
    <View style={style.wrapper}>
      <View style={style.post}>
        <FlatList
          ListHeaderComponent={header}
          ListFooterComponent={() => <CommentForm onSend={(c) => sendComment(c)} />}
          ListEmptyComponent={() => <Text style={style.warn}>This post has no comments yet</Text>}
          data={post.comments.sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))}
          renderItem={renderComment}
          keyExtractor={(comment) => comment.id}
        />
      </View>

      <DeleteModal
        confirm={isModalModePost ? onPostDelete : onCommentDelete}
        title={`Delete ${isModalModePost ? 'post' : 'comment'}`}
        message={`Are you sure you want to delete this ${isModalModePost ? 'post' : 'comment'}?`}
      />

      <EditModal
        text={isModalModePost ? post.body : curComment?.body}
        confirm={editModalConfirm}
        title={`Edit ${isModalModePost ? 'post' : 'comment'}`}
        message="Save changes?"
      />
    </View>
  ) : null;
};

const mapStateToProps = (state) => ({
  user: state.profile.user,
  ...state.post
});

const mapDispatchToProps = {
  ...actions,
  ...deleteModalActions,
  ...editModalActions,
  ...feedActions
};

export default connect(mapStateToProps, mapDispatchToProps)(ExtendedPost);
