import {
  SET_POST, ADD_COMMENT, SET_CURRENT_COMMENT, UPDATE_COMMENT, DELETE_COMMENT
} from './actionTypes';

const initialState = {
  curComment: null,
  post: null,
  error: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_POST: {
      const { post, error } = action.payload;
      return {
        ...state,
        error,
        post
      };
    }
    case SET_CURRENT_COMMENT: {
      const { id } = action.payload;
      const curComment = state.post.comments.find((c) => c.id === id);
      return {
        ...state,
        curComment
      };
    }
    case ADD_COMMENT: {
      const { comment } = action.payload;
      return {
        ...state,
        post: {
          ...state.post,
          commentCount: +state.post.commentCount + 1,
          comments: [
            ...state.post.comments,
            comment
          ]
        }
      };
    }
    case UPDATE_COMMENT: {
      const { comment } = action.payload;
      const comments = state.post.comments.map((c) => {
        if (c.id === comment.id) {
          return { ...c, ...comment };
        }
        return c;
      });
      return {
        ...state,
        post: {
          ...state.post,
          comments
        }
      };
    }
    case DELETE_COMMENT: {
      const { id } = action.payload;
      const updatedComments = state.post.comments.filter((c) => c.id !== id);
      const updatedPost = {
        ...state.post,
        commentCount: +state.post.commentCount - 1,
        comments: updatedComments
      };
      return {
        ...state,
        post: updatedPost
      };
    }
    default: {
      return state;
    }
  }
}
