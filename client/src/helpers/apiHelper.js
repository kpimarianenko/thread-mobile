import axios from 'axios';
import store from '../store';

const API_URL = 'http://10.0.2.2:3001';

export function callApi(method, options) {
  let methodFunc;
  const methodUpper = method.toUpperCase();
  switch (methodUpper) {
    case 'GET': {
      methodFunc = axios.get;
      break;
    }
    case 'POST': {
      methodFunc = axios.post;
      break;
    }
    case 'PATCH': {
      methodFunc = axios.patch;
      break;
    }
    case 'PUT': {
      methodFunc = axios.put;
      break;
    }
    case 'DELETE': {
      methodFunc = axios.delete;
      break;
    }
    default: methodFunc = axios.get;
  }

  const { token } = store.getState().profile;
  const config = options?.config || { headers: {} };
  if (token) {
    if (!config.headers) {
      config.headers = {};
    }
    config.headers.Authorization = `Bearer ${token}`;
  }

  const url = `${API_URL}${options.endpoint}`;

  if (methodUpper === 'GET' || methodUpper === 'DELETE') {
    return methodFunc(url, config);
  }
  return methodFunc(url, options.data, config);
}

export default API_URL;
