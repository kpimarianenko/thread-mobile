import { capitalizeFirstLetter } from './strHelper';
import regexp from './regexp';

const rulesMap = {
  required: (value) => !!value,
  email: (value) => regexp.email.test(value),
  minLen: (value, ruleValue) => value.length >= ruleValue,
  maxLen: (value, ruleValue) => value.length <= ruleValue
};

function getErrorMessage(ruleKey, key, ruleValue) {
  const messages = {
    required: `${key} is required`,
    email: 'Email is invalid',
    minLen: `${key} must have at least ${ruleValue} characters`,
    maxLen: `${key} must have no more than ${ruleValue} characters`
  };
  return capitalizeFirstLetter(messages[ruleKey]) || '';
}

export default function validate(rules, obj) {
  const objKeys = Object.keys(obj);

  const result = {};
  let error = '';

  objKeys.forEach((key) => {
    const keyRules = Object.keys(rules[key]);

    result[key] = keyRules.every((ruleKey) => {
      const ruleValue = rules[key][ruleKey];
      const isFieldValid = rulesMap[ruleKey](obj[key], ruleValue);
      if (!isFieldValid) {
        error = getErrorMessage(ruleKey, key, ruleValue);
      }
      return isFieldValid;
    });
  });

  return {
    getResult: () => result,
    isValid: () => Object.values(result).every((value) => value),
    getErrorMessage: () => error
  };
}
