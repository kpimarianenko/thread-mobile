import React from 'react';
import { connect } from 'react-redux';
import Modal from '../Modal';
import { hideDeleteWindow } from './actions';

const DeleteModal = ({ isShown, hideDeleteWindow, children, ...attrs }) => {
  const close = () => {
    hideDeleteWindow();
  };

  return <Modal isShown={isShown} close={close} {...attrs}>{ children }</Modal>;
};

const mapStateToProps = (state) => ({
  ...state.deleteModal
});

const mapDispatchToProps = {
  hideDeleteWindow
};

export default connect(mapStateToProps, mapDispatchToProps)(DeleteModal);
