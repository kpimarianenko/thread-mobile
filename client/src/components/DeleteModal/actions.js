import { SET_MODAL_DISPLAY } from './actionTypes';

const setModalDisplay = (isShown) => ({
  type: SET_MODAL_DISPLAY,
  payload: {
    isShown
  }
});

export const showDeleteWindow = () => setModalDisplay(true);

export const hideDeleteWindow = () => setModalDisplay(false);
