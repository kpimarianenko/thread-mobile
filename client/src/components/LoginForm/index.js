import React, { useState } from 'react';
import { View } from 'react-native';
import ErrorInput from '../ErrorInput';
import Button from '../Button';
import Error from '../Error';
import EmailImg from '../../assets/icons/email.png';
import LockImg from '../../assets/icons/lock.png';
import style from '../../styles/auth.style';
import { login } from '../../services/authService';
import validate from '../../helpers/formValidator';

const LoginForm = ({ loginSuccess, loginFailure, authorizing, authorized, isLoading, error }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [validationResults, setValidationResults] = useState(null);

  const onLogin = () => {
    const formValidationResults = validate({
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minLen: 3
      }
    }, { email, password });

    setValidationResults(formValidationResults);

    if (formValidationResults.isValid()) {
      authorizing();
      login({ email, password })
        .then((res) => loginSuccess(res.data))
        .catch((err) => loginFailure(err.response.data.message))
        .finally(authorized);
    }
  };

  return (
    <View style={style.form}>
      <ErrorInput
        error={!validationResults?.getResult().email && validationResults}
        value={email}
        onChangeText={setEmail}
        source={EmailImg}
        placeholder="Email"
      />
      <ErrorInput
        error={!validationResults?.getResult().password && validationResults}
        value={password}
        onChangeText={setPassword}
        source={LockImg}
        secureTextEntry
        placeholder="Password"
      />
      <Button loading={isLoading} onPress={onLogin} title="Login" />
      <Error style={style.error} message={validationResults?.getErrorMessage() || error} />
    </View>
  );
};

export default LoginForm;
