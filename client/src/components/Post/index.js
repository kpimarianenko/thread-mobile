import React from 'react';
import moment from 'moment';
import { View, Text, Image } from 'react-native';
import ReactionCount from '../ReactionCount';
import LikeImg from '../../assets/icons/like.png';
import DislikeImg from '../../assets/icons/dislike.png';
import CommentImg from '../../assets/icons/comment.png';
import style from './style';

const Post = ({ post, children, openDetails, setLike, setDislike }) => (
  <View style={style.post}>
    { post.image
      ? (
        <Image
          resizeMode="cover"
          source={{ uri: post.image.link }}
          style={style.image}
        />
      )
      : null}
    <View style={style.body}>
      <Text style={style.postInfo}>
        {`posted by ${post.user.username} - ${moment(post.createdAt).fromNow()}`}
      </Text>
      <Text style={style.text}>{post.body}</Text>
    </View>
    <View style={style.bottom}>
      <View style={style.reactions}>
        <ReactionCount onPress={setLike} source={LikeImg} count={post.likeCount} />
        <ReactionCount onPress={setDislike} source={DislikeImg} count={post.dislikeCount} />
        <ReactionCount onPress={openDetails} source={CommentImg} count={post.commentCount} />
      </View>
      {children}
    </View>
  </View>
);

export default Post;
