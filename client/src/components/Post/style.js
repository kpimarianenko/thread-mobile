import { StyleSheet } from 'react-native';
import baseStyle, { colors, variables } from '../../styles/base.style';

const style = StyleSheet.create({
  post: {
    ...baseStyle.container,
    overflow: 'hidden',
    marginBottom: variables.gap,
    padding: 0
  },
  bottom: {
    ...baseStyle.row,
    justifyContent: 'space-between',
    padding: variables.gap,
    borderColor: colors.border,
    borderTopWidth: 1
  },
  reactions: {
    ...baseStyle.row
  },
  comments: {
    borderColor: colors.border,
    borderTopWidth: 1,
    padding: variables.gap
  },
  text: {
    color: colors.textDark,
    marginTop: 5
  },
  header: {
    ...baseStyle.header,
    color: colors.textDarkTint,
    fontSize: variables.headersFontSize / 1.7
  },
  postInfo: {
    color: colors.textLightMuted
  },
  body: {
    padding: variables.gap
  },
  image: {
    aspectRatio: 2,
    width: '100%'
  }
});

export default style;
