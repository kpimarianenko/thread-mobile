import React, { useState } from 'react';
import { View } from 'react-native';
import ErrorInput from '../ErrorInput';
import Button from '../Button';
import Error from '../Error';
import EmailImg from '../../assets/icons/email.png';
import LockImg from '../../assets/icons/lock.png';
import UsernameImg from '../../assets/icons/username.png';
import style from '../../styles/auth.style';
import { registration } from '../../services/authService';
import validate from '../../helpers/formValidator';

const RegistrationForm = ({
  loginSuccess, loginFailure, error, isLoading, authorizing, authorized
}) => {
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [validationResults, setValidationResults] = useState(null);

  const onRegister = () => {
    const formValidationResults = validate({
      email: {
        required: true,
        email: true
      },
      username: {
        required: true,
        minLen: 3,
        maxLen: 16
      },
      password: {
        required: true,
        minLen: 3
      }
    }, { email, username, password });

    setValidationResults(formValidationResults);

    if (formValidationResults.isValid()) {
      authorizing();
      registration({ email, username, password })
        .then((res) => loginSuccess(res.data))
        .catch((err) => loginFailure(err.response.data.message))
        .finally(authorized);
    }
  };

  return (
    <View style={style.form}>
      <ErrorInput
        error={!validationResults?.getResult().email && validationResults}
        value={email}
        onChangeText={setEmail}
        source={EmailImg}
        placeholder="Email"
      />
      <ErrorInput
        error={!validationResults?.getResult().username && validationResults}
        value={username}
        onChangeText={setUsername}
        source={UsernameImg}
        placeholder="Username"
      />
      <ErrorInput
        error={!validationResults?.getResult().password && validationResults}
        value={password}
        secureTextEntry
        onChangeText={setPassword}
        source={LockImg}
        placeholder="Password"
      />
      <Button loading={isLoading} onPress={onRegister} title="Register" />
      <Error style={style.error} message={validationResults?.getErrorMessage() || error} />
    </View>
  );
};

export default RegistrationForm;
