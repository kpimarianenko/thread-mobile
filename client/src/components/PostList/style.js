import { StyleSheet } from 'react-native';
import baseStyle from '../../styles/base.style';

const style = StyleSheet.create({
  warn: {
    ...baseStyle.centerText
  }
});

export default style;
