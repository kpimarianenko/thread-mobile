import React from 'react';
import { FlatList, Text } from 'react-native';
import Post from '../Post';
import style from './style';

const PostList = ({ openDetails, posts, header, isLoading, setLike, setDislike, ...attrs }) => {
  const renderPost = ({ item }) => (
    <Post
      openDetails={() => openDetails(item.id)}
      setLike={() => setLike(item.id)}
      setDislike={() => setDislike(item.id)}
      post={item}
    />
  );

  const warn = () => (
    <>
      {!isLoading && <Text style={style.warn}>{'Sorry, but we can\'t find any post'}</Text>}
    </>
  );

  return (
    <FlatList
      {...attrs}
      ListEmptyComponent={warn}
      data={posts}
      renderItem={renderPost}
      keyExtractor={(post) => post.id}
    />
  );
};

export default PostList;
