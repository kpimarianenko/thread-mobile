import { StyleSheet } from 'react-native';
import baseStyle, { colors, variables } from '../../styles/base.style';

const style = StyleSheet.create({
  wrapper: {
    ...baseStyle.centered,
    ...baseStyle.maximized,
    ...StyleSheet.absoluteFill,
    backgroundColor: colors.modalWrapper
  },
  modal: {
    ...baseStyle.container,
    width: '80%'
  },
  title: {
    ...baseStyle.header,
    fontSize: variables.headersFontSize / 1.3
  },
  message: {
    marginTop: variables.gap / 3,
    fontSize: variables.headersFontSize / 1.6
  },
  cross: {
    fontSize: variables.iconSize,
    marginRight: variables.gap
  },
  header: {
    ...baseStyle.row,
    justifyContent: 'space-between'
  },
  controls: {
    ...baseStyle.row,
    justifyContent: 'flex-end',
    marginTop: variables.gap
  },
  button: {
    paddingHorizontal: variables.gap
  },
  cancel: {
    marginRight: variables.gap,
    color: colors.textDark,
    borderColor: colors.textDark,
    borderWidth: 1,
    backgroundColor: colors.backgroundSecondary
  },
  cancelText: {
    color: colors.textDark
  }
});

export default style;
