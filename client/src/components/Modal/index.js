import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Button from '../Button';
import style from './style';

const Modal = ({ children, title, message, isShown, close, confirm }) => (
  isShown ? (
    <View style={style.wrapper}>
      <View style={style.modal}>
        <View style={style.header}>
          <Text style={style.title}>{title}</Text>
          <TouchableOpacity onPress={close}>
            <Text style={style.cross}>x</Text>
          </TouchableOpacity>
        </View>
        <Text style={style.message}>{message}</Text>
        <View style={style.body}>{children}</View>
        <View style={style.controls}>
          <Button style={[style.cancel, style.button]} textStyle={style.cancelText} title="No" onPress={close} />
          <Button style={style.button} title="Yes" onPress={confirm} />
        </View>
      </View>
    </View>
  ) : null
);

export default Modal;
