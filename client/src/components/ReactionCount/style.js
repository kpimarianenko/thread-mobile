import { StyleSheet } from 'react-native';
import baseStyle, { variables } from '../../styles/base.style';

const style = StyleSheet.create({
  reaction: {
    ...baseStyle.row,
    paddingRight: 5,
    marginRight: variables.gap / 1.5
  },
  image: {
    width: variables.iconSize,
    height: variables.iconSize,
    marginRight: variables.gap / 3
  }
});

export default style;
