import React from 'react';
import { Text, Image, TouchableOpacity } from 'react-native';
import style from './style';

const ReactionCount = ({ source, count, ...attrs }) => (
  <TouchableOpacity {...attrs} style={style.reaction}>
    <Image source={source} style={style.image} />
    <Text style={style.count}>{ count || 0}</Text>
  </TouchableOpacity>
);

export default ReactionCount;
