import { StyleSheet } from 'react-native';
import baseStyle, { colors, variables } from '../../styles/base.style';

const style = StyleSheet.create({
  attachImage: {
    paddingHorizontal: variables.gap * 2,
    backgroundColor: colors.secondary
  },
  form: {
    ...baseStyle.container,
    marginBottom: variables.gap
  },
  textArea: {
    ...baseStyle.container
  },
  controls: {
    ...baseStyle.row,
    marginTop: variables.gap,
    justifyContent: 'space-between'
  }
});

export default style;
