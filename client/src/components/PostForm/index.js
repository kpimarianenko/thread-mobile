import React, { useState } from 'react';
import DocumentPicker from 'react-native-document-picker';
import { View, TextInput } from 'react-native';
import Button from '../Button';
import style from './style';
import { createPost } from '../../services/postService';
import Error from '../Error';

const PostForm = ({
  addPost, setError, isLoading, startPostsLoading, postsLoaded, error, user
}) => {
  const [message, setMessage] = useState('');
  const [image, setImage] = useState(null);

  const openUploadMenu = async () => {
    const res = await DocumentPicker.pick({
      type: [DocumentPicker.types.allFiles],
      readContent: true
    });
    return res;
  };

  const sendMessagePressHandler = () => {
    if (message.length > 0) {
      startPostsLoading();

      // postImage(image)
      //   .then((res) => {
      //     const { status, message } = res;
      //     if (status || message) setError(message);
      //     else {
      //       setError('');
      //     }
      //   })
      //   .catch((err) => setError(err.message))
      //   .finally(postsLoaded);

      createPost(message, image)
        .then((res) => addPost({
          ...res.data,
          user
        }))
        .catch((err) => setError(err.response.data.message))
        .finally(postsLoaded);
    }
  };

  const fileUploadHandler = () => {
    openUploadMenu()
      .then((res) => setImage(res))
      .catch(() => {});
  };

  return (
    <>
      <View style={style.form}>
        <TextInput
          value={message}
          onChangeText={setMessage}
          placeholder="What is the news?"
          style={style.textArea}
          multiline
          numberOfLines={3}
          textAlignVertical="top"
        />
        <View style={style.controls}>
          <Button disabled={!!image} onPress={fileUploadHandler} style={style.attachImage} title="Attach image" />
          <Button loading={isLoading} onPress={sendMessagePressHandler} style={style.button} title="Post" />
        </View>
      </View>
      <Error message={error} />
    </>
  );
};

export default PostForm;
