import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import moment from 'moment';
import EditImg from '../../assets/icons/edit.png';
import DeleteImg from '../../assets/icons/delete.png';
import LikeImg from '../../assets/icons/like.png';
import DislikeImg from '../../assets/icons/dislike.png';
import style from './style';
import ReactionCount from '../ReactionCount';

const Comment = ({ comment, user, setLike, setDislike, onEdit, onDelete }) => (
  <View style={style.comment}>
    <View style={style.head}>
      <View style={style.row}>
        <Text style={style.username}>{comment.user.username}</Text>
        <Text style={style.date}>{moment(comment.createdAt).fromNow()}</Text>
      </View>
      { user.id === comment.user.id
        ? (
          <View style={style.row}>
            <TouchableOpacity onPress={onEdit}>
              <Image style={style.icon} source={EditImg} />
            </TouchableOpacity>
            <TouchableOpacity onPress={onDelete}>
              <Image style={style.icon} source={DeleteImg} />
            </TouchableOpacity>
          </View>
        ) : null }
    </View>
    <Text style={style.body}>{comment.body}</Text>
    <View style={style.row}>
      <ReactionCount onPress={setLike} source={LikeImg} count={comment.likeCount} />
      <ReactionCount onPress={setDislike} source={DislikeImg} count={comment.dislikeCount} />
    </View>
  </View>
);

export default Comment;
