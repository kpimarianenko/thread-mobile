import { StyleSheet } from 'react-native';
import baseStyle, { colors, variables } from '../../styles/base.style';

const style = StyleSheet.create({
  comment: {
    marginBottom: variables.gap * 2
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  head: {
    ...baseStyle.row,
    justifyContent: 'space-between'
  },
  icon: {
    width: variables.iconSize,
    height: variables.iconSize,
    marginLeft: variables.gap
  },
  username: {
    fontSize: variables.headersFontSize / 1.7,
    marginRight: variables.gap / 2
  },
  body: {
    marginVertical: variables.gap / 2
  },
  date: {
    color: colors.textLightMuted
  }
});

export default style;
