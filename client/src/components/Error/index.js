import React from 'react';
import { Text } from 'react-native';
import style from './style';

const Error = ({ style: additionalStyle, message, ...attrs }) => (
  message ? <Text {...attrs} style={[style.error, additionalStyle]}>{ message }</Text> : null
);

export default Error;
