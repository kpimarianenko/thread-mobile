import { StyleSheet } from 'react-native';
import baseStyle, { colors } from '../../styles/base.style';

const style = StyleSheet.create({
  error: {
    color: colors.danger,
    ...baseStyle.centerText
  }
});

export default style;
