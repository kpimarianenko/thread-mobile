import React from 'react';
import {
  Image, Text, TouchableOpacity, View
} from 'react-native';
import Post from '../Post';
import EditImg from '../../assets/icons/edit.png';
import DeleteImg from '../../assets/icons/delete.png';
import style from './style';

const CommentsListHeader = ({ onEdit, onDelete, post, user, setLike, setDislike }) => (
  <>
    <Post setLike={setLike} setDislike={setDislike} post={post}>
      {
        user.id === post.user.id ? (
          <View style={style.controls}>
            <TouchableOpacity onPress={onEdit}>
              <Image style={style.icon} source={EditImg} />
            </TouchableOpacity>
            <TouchableOpacity onPress={onDelete}>
              <Image style={style.icon} source={DeleteImg} />
            </TouchableOpacity>
          </View>
        ) : null
      }
    </Post>
    <Text style={style.commentsHeader}>Comments</Text>
  </>
);

export default CommentsListHeader;
