import { StyleSheet } from 'react-native';
import baseStyle, { colors, variables } from '../../styles/base.style';

const style = StyleSheet.create({
  commentsHeader: {
    ...baseStyle.header,
    fontSize: variables.headersFontSize / 1.5,
    borderColor: colors.border,
    marginBottom: variables.gap,
    borderBottomWidth: 1
  },
  controls: {
    ...baseStyle.row
  },
  icon: {
    width: variables.iconSize,
    height: variables.iconSize,
    marginLeft: variables.gap
  }
});

export default style;
