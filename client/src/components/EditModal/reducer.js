import { SET_MODAL_DISPLAY } from './actionTypes';

const initialState = {
  isShown: false
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_MODAL_DISPLAY: {
      const { isShown } = action.payload;
      return {
        ...state,
        isShown
      };
    }
    default: {
      return state;
    }
  }
}
