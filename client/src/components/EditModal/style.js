import { StyleSheet } from 'react-native';
import baseStyle, { variables } from '../../styles/base.style';

const style = StyleSheet.create({
  input: {
    ...baseStyle.container,
    marginTop: variables.gap
  }
});

export default style;
