import React, { useEffect, useState } from 'react';
import { TextInput } from 'react-native';
import { connect } from 'react-redux';
import Modal from '../Modal';
import { hideEditWindow } from './actions';
import style from './style';

const EditModal = ({ text, confirm, isShown, hideEditWindow, children, ...attrs }) => {
  const [value, setValue] = useState('');

  const onConfirm = () => {
    confirm(value);
  };

  const close = () => {
    hideEditWindow();
  };

  useEffect(() => {
    setValue(text);
    return () => {
      setValue('');
    };
  }, [isShown]);

  return (
    <Modal confirm={onConfirm} isShown={isShown} close={close} {...attrs}>
      <TextInput
        style={style.input}
        multiline
        value={value}
        onChangeText={setValue}
      />
    </Modal>
  );
};

const mapStateToProps = (state) => ({
  ...state.editModal
});

const mapDispatchToProps = {
  hideEditWindow
};

export default connect(mapStateToProps, mapDispatchToProps)(EditModal);
