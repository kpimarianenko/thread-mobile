import { SET_MODAL_DISPLAY } from './actionTypes';

const setModalDisplay = (isShown) => ({
  type: SET_MODAL_DISPLAY,
  payload: {
    isShown
  }
});

export const showEditWindow = () => setModalDisplay(true);

export const hideEditWindow = () => setModalDisplay(false);
