import { StyleSheet } from 'react-native';
import baseStyle, { colors, variables } from '../../styles/base.style';

const style = StyleSheet.create({
  form: {
    ...baseStyle.row
  },
  input: {
    ...baseStyle.maximized,
    borderColor: colors.border,
    marginRight: variables.gap,
    borderWidth: 1
  }
});

export default style;
