import React, { useState } from 'react';
import { TextInput, View } from 'react-native';
import Button from '../Button';
import style from './style';

const CommentForm = ({ onSend, ...attrs }) => {
  const [value, setValue] = useState('');

  return (
    <View style={style.form}>
      <TextInput value={value} onChangeText={setValue} style={style.input} {...attrs} />
      <Button title="Send" onPress={() => onSend(value)} />
    </View>
  );
};

export default CommentForm;
