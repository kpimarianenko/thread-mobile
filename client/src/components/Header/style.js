import { StyleSheet } from 'react-native';
import baseStyle, { colors, variables } from '../../styles/base.style';

const style = StyleSheet.create({
  wrapper: {
    ...baseStyle.row,
    backgroundColor: colors.backgroundSecondary,
    paddingVertical: variables.gap,
    paddingHorizontal: variables.gap * 2,
    justifyContent: 'space-between'
  },
  profile: {
    ...baseStyle.row
  },
  username: {
    ...baseStyle.header,
    fontSize: variables.smallHeadersFontSize
  },
  navigation: {
    ...baseStyle.row
  },
  navIcon: {
    width: 25,
    height: 25,
    marginLeft: variables.gap
  },
  ava: {
    width: 45,
    height: 45,
    borderRadius: 100,
    marginRight: variables.gap
  }
});

export default style;
