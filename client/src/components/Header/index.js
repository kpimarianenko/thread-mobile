import React from 'react';
import { connect } from 'react-redux';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import AvaPlaceholderImg from '../../assets/images/ava-placeholder.png';
import ProfileImg from '../../assets/icons/profile.png';
import LogoutImg from '../../assets/icons/logout.png';
import { logout } from '../../screens/Profile/actions';
import routes from '../../routes/routeNames';
import style from './style';

const Header = ({ navigation, logout, user }) => {
  const moveToProfile = () => {
    navigation.navigate(routes.main.PROFILE);
  };

  return (
    <View style={style.wrapper}>
      <View style={style.profile}>
        <Image style={style.ava} source={AvaPlaceholderImg} />
        <Text style={style.username}>{user.username}</Text>
      </View>
      <View style={style.navigation}>
        <TouchableOpacity onPress={moveToProfile}>
          <Image style={style.navIcon} source={ProfileImg} />
        </TouchableOpacity>
        <TouchableOpacity onPress={logout}>
          <Image style={style.navIcon} source={LogoutImg} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  ...state.profile
});

const mapDispatchToProps = {
  logout
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
