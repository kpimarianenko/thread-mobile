import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import style from './style';

const ToggleAuthScreen = ({ text, btnText, onPress }) => (
  <View style={style.wrapper}>
    <Text style={style.text}>
      { text }
      {' '}
    </Text>
    <TouchableOpacity onPress={onPress}>
      <Text style={style.button}>{ btnText }</Text>
    </TouchableOpacity>
  </View>
);

export default ToggleAuthScreen;
