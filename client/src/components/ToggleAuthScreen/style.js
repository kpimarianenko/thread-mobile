import { StyleSheet } from 'react-native';
import baseStyle, { colors } from '../../styles/base.style';

const style = StyleSheet.create({
  wrapper: {
    ...baseStyle.container,
    ...baseStyle.row,
    justifyContent: 'center',
    width: '100%'
  },
  button: {
    color: colors.primary
  }
});

export default style;
