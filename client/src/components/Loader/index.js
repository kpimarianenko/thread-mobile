import React from 'react';
import { ActivityIndicator } from 'react-native';

const Loader = ({ style, ...attrs }) => (
  <ActivityIndicator {...attrs} style={style} color={style?.color || 'black'} size={style?.size} />
);

export default Loader;
