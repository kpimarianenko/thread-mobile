import { StyleSheet } from 'react-native';
import baseStyle, { colors, variables } from '../../styles/base.style';

const style = StyleSheet.create({
  button: {
    borderRadius: variables.borderRadius,
    backgroundColor: colors.primary,
    padding: 10
  },
  title: {
    ...baseStyle.centerText,
    ...baseStyle.header,
    fontSize: variables.smallHeadersFontSize,
    color: colors.textLight
  },
  loader: {
    color: colors.textLight,
    margin: 3
  }
});

export default style;
