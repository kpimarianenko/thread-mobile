import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Loader from '../Loader';
import style from './style';

const Button = ({ title, textStyle, loading, style: additionalStyle, ...attrs }) => (
  <TouchableOpacity disabled={loading} {...attrs} style={[style.button, additionalStyle]}>
    { loading
      ? <Loader style={style.loader} />
      : <Text style={[style.title, textStyle]}>{ title }</Text> }
  </TouchableOpacity>
);

export default Button;
