import React from 'react';
import { View, Text, Image } from 'react-native';
import LogoImg from '../../assets/images/logo.png';
import style from './style';

const Logo = () => (
  <View style={style.logoWrapper}>
    <Image style={style.logoImg} source={LogoImg} />
    <Text style={style.logoName}>Thread</Text>
  </View>
);

export default Logo;
