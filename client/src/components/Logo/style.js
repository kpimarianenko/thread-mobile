import { StyleSheet } from 'react-native';
import baseStyle, { colors, variables } from '../../styles/base.style';

const style = StyleSheet.create({
  logoImg: {
    height: 70,
    width: 70,
    marginRight: variables.gap
  },
  logoName: {
    color: colors.textDarkTint,
    ...baseStyle.header
  },
  logoWrapper: {
    ...baseStyle.row
  }
});

export default style;
