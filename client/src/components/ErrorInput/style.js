import { StyleSheet } from 'react-native';
import baseStyle, { colors, variables } from '../../styles/base.style';

const style = StyleSheet.create({
  wrapper: {
    ...baseStyle.container,
    ...baseStyle.row,
    marginBottom: variables.gap,
    padding: 0
  },
  image: {
    width: variables.smallHeadersFontSize,
    height: variables.smallHeadersFontSize,
    marginHorizontal: variables.gap - 2
  },
  input: {
    width: '100%',
    paddingLeft: 0
  },
  error: {
    borderColor: colors.danger,
    backgroundColor: colors.dangerTint
  }
});

export default style;
