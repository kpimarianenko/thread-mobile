import React from 'react';
import { View, TextInput, Image } from 'react-native';
import style from './style';

const ErrorInput = ({ error, source, ...attrs }) => (
  <View style={[style.wrapper, error ? style.error : null]}>
    <Image style={style.image} source={source} />
    <TextInput {...attrs} style={style.input} />
  </View>
);

export default ErrorInput;
