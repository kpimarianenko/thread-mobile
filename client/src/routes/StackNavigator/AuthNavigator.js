import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../../screens/Login';
import routes from '../routeNames';
import Registration from '../../screens/Registration';

const Stack = createStackNavigator();

function AuthNavigator() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen
        name={routes.auth.LOGIN}
        component={Login}
      />
      <Stack.Screen
        name={routes.auth.REGISTRATION}
        component={Registration}
      />
    </Stack.Navigator>
  );
}

export default AuthNavigator;
