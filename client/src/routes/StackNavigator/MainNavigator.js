import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Feed from '../../screens/Feed';
import Profile from '../../screens/Profile';
import Header from '../../components/Header';
import routes from '../routeNames';
import ExtendedPost from '../../screens/ExtendedPost';

const Stack = createStackNavigator();

function AuthNavigator() {
  const header = ({ navigation }) => <Header navigation={navigation} />;

  return (
    <Stack.Navigator screenOptions={{ header }}>
      <Stack.Screen
        name={routes.main.FEED}
        component={Feed}
      />
      <Stack.Screen
        name={routes.main.PROFILE}
        component={Profile}
      />
      <Stack.Screen
        name={routes.main.POST}
        component={ExtendedPost}
      />
    </Stack.Navigator>
  );
}

export default AuthNavigator;
