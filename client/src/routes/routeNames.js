const routes = {
  main: {
    POST: 'Post',
    FEED: 'Feed',
    PROFILE: 'Profile'
  },

  auth: {
    LOGIN: 'Login',
    REGISTRATION: 'Registration'
  }
};

export default routes;
