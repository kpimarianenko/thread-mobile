import React from 'react';
import { connect } from 'react-redux';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import AuthNavigator from './StackNavigator/AuthNavigator';
import MainNavigator from './StackNavigator/MainNavigator';

function AppNavigator({ user }) {
  return (
    <NavigationContainer>
      { user ? <MainNavigator /> : <AuthNavigator />}
    </NavigationContainer>
  );
}

const mapStateToProps = (state) => ({
  ...state.profile
});

export default connect(mapStateToProps)(AppNavigator);
