import { callApi } from '../helpers/apiHelper';

export const addComment = async (body, postId) => {
  const response = await callApi('POST', {
    endpoint: '/api/comments',
    data: {
      body,
      postId
    }
  });
  return response;
};

export const updateComment = async (id, body) => {
  const response = await callApi('PUT', {
    endpoint: `/api/comments/${id}`,
    data: {
      body
    }
  });
  return response;
};

export const deleteComment = async (commentId) => {
  const response = await callApi('DELETE', {
    endpoint: `/api/comments/${commentId}`
  });
  return response;
};

export const setReaction = async (commentId, isLike) => {
  const response = await callApi('PUT', {
    endpoint: '/api/comments/react',
    data: {
      commentId,
      isLike
    }
  });
  return response;
};
