import { callApi } from '../helpers/apiHelper';

export const login = async (data) => {
  const response = await callApi('POST', {
    endpoint: '/api/auth/login',
    data
  });
  return response;
};

export const registration = async (data) => {
  const response = await callApi('POST', {
    endpoint: '/api/auth/register',
    data
  });
  return response;
};
