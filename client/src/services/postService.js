import { callApi } from '../helpers/apiHelper';

export const getPosts = async (filter) => {
  const response = await callApi('GET', {
    endpoint: '/api/posts',
    config: {
      params: filter
    }
  });
  return response;
};

export const getPostById = async (id) => {
  const response = await callApi('GET', {
    endpoint: `/api/posts/${id}`
  });
  return response;
};

export const createPost = async (body) => {
  const response = await callApi('POST', {
    endpoint: '/api/posts',
    data: {
      body
    }
  });
  return response;
};

export const updatePost = async (id, body) => {
  const response = await callApi('PUT', {
    endpoint: `/api/posts/${id}`,
    data: {
      body
    }
  });
  return response;
};

export const deletePost = async (id) => {
  const response = await callApi('DELETE', {
    endpoint: `/api/posts/${id}`
  });
  return response;
};

export const setReaction = async (postId, isLike) => {
  const response = await callApi('PUT', {
    endpoint: '/api/posts/react',
    data: {
      postId,
      isLike
    }
  });
  return response;
};

// export const postImage = async (image) => {
//   const { token } = store.getState().profile;
//   const ret = await RNFetchBlob.fetch(
//     'POST',
//     'http://10.0.2.2:3001/api/images',
//     {
//       'Content-Type': 'multipart/form-data',
//       Authorization: `Bearer ${token}`
//     },
//     [
//       {
//         name: 'image',
//         filename: `${Date.now()}.png`,
//         type: 'image/png',
//         data: RNFetchBlob.wrap(image.uri)
//       }
//     ]
//   );
//   return JSON.parse(ret.data);
// };

export const postImage = async (image) => {
  const data = new FormData();

  data.append('image', {
    uri: image.uri,
    type: image.type,
    name: image.fileName
  });

  const response = await callApi('POST', {
    endpoint: '/api/images',
    data,
    config: {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
  });
  return response;
};
