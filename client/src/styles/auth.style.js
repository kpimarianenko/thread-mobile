import { StyleSheet } from 'react-native';
import baseStyle, { colors, variables } from './base.style';

const style = StyleSheet.create({
  page: {
    ...baseStyle.maximized,
    ...baseStyle.centered,
    padding: variables.gap
  },
  title: {
    marginTop: variables.gap,
    color: colors.secondary,
    ...baseStyle.centerText,
    ...baseStyle.header
  },
  form: {
    ...baseStyle.container,
    marginVertical: variables.gap,
    width: '100%'
  },
  error: {
    ...baseStyle.centerText,
    marginTop: variables.gap
  }
});

export default style;
