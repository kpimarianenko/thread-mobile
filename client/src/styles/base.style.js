import { StyleSheet } from 'react-native';

export const colors = {
  border: '#e3e3e3',
  textDark: '#3c3c3c',
  textDarkTint: '#6c7884',
  textLight: '#ffffff',
  textLightMuted: '#cfcfcf',
  primary: '#177bc6',
  secondary: '#55b4a4',
  backgroundPrimary: '#efefef',
  backgroundSecondary: '#ffffff',
  danger: '#f26262',
  dangerTint: '#fff0eb',
  modalWrapper: '#0005'
};

export const variables = {
  headersFontSize: 30,
  smallHeadersFontSize: 20,
  borderRadius: 5,
  iconSize: 15,
  gap: 14
};

const baseStyle = StyleSheet.create({
  container: {
    backgroundColor: colors.backgroundSecondary,
    borderRadius: variables.borderRadius,
    borderWidth: 1,
    borderColor: colors.border,
    padding: variables.gap
  },
  maximized: {
    flex: 1
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  centered: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  centerText: {
    textAlign: 'center'
  },
  header: {
    fontSize: variables.headersFontSize,
    fontWeight: 'bold'
  }
});

export default baseStyle;
