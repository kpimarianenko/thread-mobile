import { combineReducers } from 'redux';
import profile from '../screens/Profile/reducer';
import feed from '../screens/Feed/reducer';
import post from '../screens/ExtendedPost/reducer';
import editModal from '../components/EditModal/reducer';
import deleteModal from '../components/DeleteModal/reducer';

const rootReducer = combineReducers({
  profile,
  feed,
  post,
  editModal,
  deleteModal
});

export default rootReducer;
